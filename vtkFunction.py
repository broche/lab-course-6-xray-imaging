# -*- coding: utf-8 -*-
"""
Created on Mon Jan 16 17:26:54 2017

@author: broche
"""

import numpy as np
import vtk


# Function to perfom a marching cube on a binary image, smooth it and render it using VTK
def renderMask(array):
    flag_mesh = True
    volume_render = VTK_Render()
    volume_render.init_all_VolumeRendering_component(flag_mesh)
    volume_render.import_numpy_array(array, 0, 1)
    volume_render.MarchingCube([0.5])
    volume_render.SmoothMesh(5, 0.5)
    volume_render.update_mapper()
    if flag_mesh:
        volume_render.add_PolyActor()
    else:
        volume_render.add_volume_to_renderer()
    volume_render.update_renderer()
    volume_render.launch_render()


class VTK_Render():
    def __init__(self, parent = None):
        self.renWin =  vtk.vtkRenderWindow()

    @staticmethod
    def exitCheck(obj, event):

        if obj.GetEventPending() != 0:
            obj.SetAbortRender(1)

    @staticmethod
    def image_float_to_int8(vol, minValue, maxValue):
        """
        Format the volume to a 0-255 image uint16
        """
        vol = (255 * (vol - minValue)) / (maxValue - minValue)
        vol = vol.astype(np.uint8)
        vol[vol < 0] = 0
        vol[vol > 255] = 255

        return vol

    def MarchingCube(self,list_thresholdValue):

        thresholdValue = int((255.0 * (list_thresholdValue[0] - self.minValue)) / (self.maxValue - self.minValue))
        threshold = vtk.vtkImageThreshold()
        threshold.SetInputConnection(self.data_importer.GetOutputPort())
        threshold.ThresholdByLower(thresholdValue)  # remove all soft tissue
        threshold.ReplaceInOn()
        threshold.SetInValue(0)  # set all values below 400 to 0
        threshold.ReplaceOutOn()
        threshold.SetOutValue(1)  # set all values above 400 to 1
        threshold.Update()

        print('Meshing')

        self.dmc = vtk.vtkMarchingCubes()
        self.dmc.SetInputConnection(threshold.GetOutputPort())
        self.dmc.ComputeNormalsOn()
        self.dmc.GenerateValues(1, 1, 1)
        self.dmc.Update()



    def SmoothMesh(self,nbIter,RelaxFactor):
        print("Smoother")
        smoother = vtk.vtkSmoothPolyDataFilter()
        smoother.SetInputConnection(self.dmc.GetOutputPort())
        smoother.SetNumberOfIterations(nbIter)
        smoother.SetRelaxationFactor(RelaxFactor)#this has little effect on the error!
        smoother.FeatureEdgeSmoothingOff()
        smoother.BoundarySmoothingOn()

        self.dmc = vtk.vtkPolyDataNormals()
        self.dmc.SetInputConnection(smoother.GetOutputPort())
        self.dmc.Update()

    def DecimateMesh(self,targeReduction):

        print("Decimate")

        decimate = vtk.vtkDecimatePro()
        decimate.SetInputConnection(self.dmc.GetOutputPort())
        decimate.SetTargetReduction(targeReduction)
        decimate.Update()

        self.dmc = vtk.vtkPolyDataNormals()
        self.dmc.SetInputConnection(decimate.GetOutputPort())
        self.dmc.Update()


    def init_all_VolumeRendering_component(self, flagMesh):
        self.flagMesh = flagMesh
        self.flagCurvature = flagMesh
        self.ren= vtk.vtkRenderer()
        self.renWin.AddRenderer(self.ren)
        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)

        self.ren.GetVolumes()

        self.alpha_channel_function = vtk.vtkPiecewiseFunction()
        self.alpha_channel_function.AddPoint(0, 0.0, 0.5, 0.0)
        self.alpha_channel_function.AddPoint(255, 1, 0.5, 0.0)

        self.color_function = vtk.vtkColorTransferFunction()
        self.color_function.AddRGBPoint(0, 0, 0.0, 0.0, 0.5, 0.0)
        self.color_function.AddRGBPoint(255, 1, 1, 1, 0.5, 0.0)

        self.volume_property = vtk.vtkVolumeProperty()
        self.volume_property.SetColor(self.color_function)
        self.volume_property.SetScalarOpacity(self.alpha_channel_function)

        self.data_importer = vtk.vtkImageImport()

        if (self.flagMesh or self.flagCurvature):
            self.volume_mapper = vtk.vtkPolyDataMapper()
        else:
            self.volume_mapper = vtk.vtkFixedPointVolumeRayCastMapper()

        self.volume = vtk.vtkVolume()



    def import_numpy_array(self, np_array,minValue,maxValue):

        self.minValue = minValue
        self.maxValue = maxValue


        np_array = self.image_float_to_int8(np_array, minValue, maxValue)
        self.shape_data = np_array.shape
        self.data_importer.CopyImportVoidPointer(np_array, np_array.nbytes)
        self.data_importer.SetDataScalarTypeToUnsignedChar()
        self.data_importer.SetNumberOfScalarComponents(1)
        self.data_importer.SetDataExtent(0, self.shape_data[2] - 1, 0, self.shape_data[1] - 1, 0,self.shape_data[0] - 1)
        self.data_importer.SetWholeExtent(0, self.shape_data[2] - 1, 0, self.shape_data[1] - 1, 0,self.shape_data[0] - 1)

    def reset_alpha_channel(self):
        self.alpha_channel_function.RemoveAllPoints()

    def reset_color_channel(self):
        self.color_function.RemoveAllPoints()

    def set_color_channel(self, value, R, G, B, mid = 0.5, sharp = 0.0):

        if self.flagCurvature:
            self.color_function.AddRGBPoint(value, R, G, B, mid, sharp)
        else:
            self.color_function.AddRGBPoint(value*255, R, G, B,mid, sharp)

    def set_alpha_channel(self,value,alpha, mid = 0.5, sharp = 0.0):
        self.alpha_channel_function.AddPoint(value*255, alpha,mid, sharp)

    def set_volume_property(self, shade=True, ambient=0.1, diffuse=0.9, specular=0.2, specular_pw=10.0,
            opacity_unit_dist=0.8919):

        self.volume_property.SetColor(self.color_function)
        self.volume_property.SetScalarOpacity(self.alpha_channel_function)

        if shade:
            self.volume_property.ShadeOn()
        else:
            self.volume_property.ShadeOff()

        self.volume_property.SetAmbient(ambient)
        self.volume_property.SetDiffuse(diffuse)
        self.volume_property.SetSpecular(specular)
        self.volume_property.SetSpecularPower(specular_pw)
        self.volume_property.SetScalarOpacityUnitDistance(opacity_unit_dist)

    def update_mapper(self, blend_mode='Composite'):


        if not self.flagMesh:
            if blend_mode == 'Composite':
                self.volume_mapper.SetBlendModeToComposite()
            elif blend_mode == 'MaxIntensity':
                self.volume_mapper.SetBlendModeToMaximumIntensity()

        self.volume_mapper.RemoveAllInputs()

        if self.flagMesh:
            self.volume_mapper.SetInputConnection(self.dmc.GetOutputPort())
        else:
            self.volume_mapper.SetInputConnection(self.data_importer.GetOutputPort())
            self.volume_mapper.AutoAdjustSampleDistancesOn()
            self.volume_mapper.IntermixIntersectingGeometryOn()
            self.volume.SetMapper(self.volume_mapper)
            self.volume.SetProperty(self.volume_property)


    def add_PolyActor(self):

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.volume_mapper)
        self.ren.AddActor(self.actor)


    def add_volume_to_renderer(self):
        self.ren.AddVolume(self.volume)


    def update_renderer(self, background=[255.0, 255.0, 255.0], sizeW = [1600,1200]):
        self.ren.SetBackground(1,1,1)
        self.renWin.SetSize(sizeW[0],sizeW[1])
        self.renWin.AddObserver("AbortCheckEvent", self.exitCheck)

    def launch_render(self):
        self.iren.SetDesiredUpdateRate(25)
        self.iren.Initialize()
        self.renWin.Render()
        self.iren.Start()
        self.close_window()
        del self.renWin, self.iren

    def close_window(self):
        self.renWin.Finalize()
        self.iren.TerminateApp()
