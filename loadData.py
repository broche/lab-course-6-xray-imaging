
import os
from PIL import Image
import numpy as np

# Simple function to load all the projections, darks and flats in an numpy array
def loadData(path):
    path_dark = path + "Darks_000\\"
    path_ref = path + "Refs_000\\"
    path_prj = path + "Projs_000\\"

    list_files_dark = os.listdir(path_dark)
    list_files_ref = os.listdir(path_ref)
    list_files_prj = os.listdir(path_prj)
    list_files_prj.sort()

    image = np.array(Image.open(path_dark + list_files_dark[0]))
    nb_darks = len(list_files_dark)
    nb_refs = len(list_files_ref)
    nb_prjs = len(list_files_prj)

    nb_columns = image.shape[0]
    nrows = image.shape[1]

    array_dark = np.zeros((nb_darks, nb_columns,nrows))
    array_ref = np.zeros((nb_refs, nb_columns, nrows))
    array_prj = np.zeros((nb_prjs, nb_columns, nrows))

    for i in range(0,nb_darks):
        path_dark_image = list_files_dark[i]
        image = Image.open(path_dark+path_dark_image)
        array_dark[i] = np.array(image)

    for i in range(0,nb_refs):
        path_ref_image = list_files_ref[i]
        image = Image.open(path_ref+path_ref_image)
        array_ref[i] = np.array(image)

    for i in range(0,nb_prjs):
        path_prjs_image = list_files_prj[i]
        image = Image.open(path_prj+path_prjs_image)
        array_prj[i] = np.array(image)


    return [array_dark,array_ref,array_prj]